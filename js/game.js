let nilaiP1 = 0;
let nilaiP2 = 0;
const ronde = 5;
let angkaRandom = Math.floor(Math.random() * 3) + 1; //angka 1-3

//func tebak benar dan salah
const tebakan = (p1, p2) => {
  if (p1 == angkaRandom) {
    nilaiP1 = nilaiP1 + 1;
    alert("pemain 1 benar");
  } else if (p2 == angkaRandom) {
    nilaiP2 = nilaiP2 + 1;
    alert("pemain 2 benar");
  } else {
    alert("pemain 1 dan pemain 2 seri");
  }
};
//confirm mengulang game
const ulangiGame = () => {
  if (confirm("apakan ingin memulai game kembali ?")) {
    playGame();
  } else {
    alert("Terimakasih");
  }
};
//func game memasukan angka 1-3, angka yang dipilih tidak boleh sama, dilakukan selama 5 ronde
const playGame = () => {
  for (let index = 1; index <= ronde; index++) {
    const angkaP1 = prompt(`Ronde ${index} Pemain 1 masukan angka 1-3 :`);
    const angkaP2 = prompt(`Ronde ${index} Pemain 2 masukan angka 1-3 :`);
    if (angkaP1 == angkaP2) {
      alert("pilihan tidak boleh sama");
      //memulai kembali angka pilihan
      index = index - 1;
    } else {
      tebakan(angkaP1, angkaP2);
    }
  }
  //hasil game
  if (nilaiP1 > nilaiP2) {
    alert("Pemain 1 menang point : " + nilaiP1);
  } else if (nilaiP1 < nilaiP2) {
    alert("Pemain 2 menang point : " + nilaiP2);
  } else {
    alert("Pemain 2 dan Pemain 1 seri");
  }

  ulangiGame();
};

//memulai game
playGame();
