import { dataTableUser, headerTableUser } from "./table-user.js";

function createTable(url, cb) {
    //header table
    let headerTable = ["id", "name", "username", "email", "city", "website"];
    headerTableUser(headerTable);
    fetch(url)
        .then((response) => response.json())
        .then((data) => cb(data))
        .catch((err) => console.log(err));
}
const cb = (data) => {
    document.getElementById("search").addEventListener("keyup", function() {
        let newData = data.filter((d) => {
            let name = d.name.toLowerCase();
            let filter = document.getElementById("search").value.toLowerCase();
            return name.includes(filter);
        });
        displayData(newData);
    });
    displayData(data);
};

const displayData = (data) => {
    let dataUser = [];
    data.forEach((d) => {
        dataUser.push([
            d.id,
            d.name,
            d.username,
            d.email,
            d.address.city,
            d.website,
        ]);
    });
    dataTableUser(dataUser);
};

const url = "https://jsonplaceholder.typicode.com/users";
createTable(url, cb);