class Table {
  constructor(init) {
    this.init = init;
  }
  createHeader(data) {
    let isi = "";
    data.forEach((d) => {
      isi += `<th>${d}</th>`;
    });
    return `<tr>
                    ${isi}
                </tr>
            `;
  }
  createDataTable(data) {
    let rowData = "";
    for (let i = 0; i < data.length; data++) {
      data.forEach((d) => {
        let isi = "";
        d.forEach((val) => {
          isi += `<td>${val}</td>`;
        });
        rowData += `<tr>${isi}</tr>`;
      });
    }
    return rowData;
  }

 
  render(element) {
    let text =
      "<table class='table table-bordered '> <thead>" +
      this.createHeader(this.init.colums) +
      "</thead><tbody>" +
      this.createDataTable(this.init.data) +
      "</tbody></table>";
    element.innerHTML = text;
  }
}

const createTable = (colums, data) => {
  const table = new Table({
    colums: colums,
    data: data,
  });

  table.render(document.getElementById("table"));
};



export default createTable;
