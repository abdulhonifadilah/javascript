// Prettier ignore
document.getElementById(
    "table"
).innerHTML = `<table class="table table-bordered">
  <thead><tr id="header-table"></tr></thead>
  <tbody id="body-table"></tbody>
</table>
<p class='text-center mt-2 fs-5' id='table-loading'>Loading...</p><p class='text-center mt-2 fs-5 text-danger' id="data-kosong">Data Kosong</p>`;
const headerTableUser = (data) => {
    let isi = "";
    data.forEach((d) => {
        isi += `<th>${d}</th>`;
    });
    document.getElementById("header-table").innerHTML = isi;
    document.getElementById("data-kosong").style.display = "none";
};

// let newData = [];
const dataTableUser = (data) => {
    document.getElementById("data-kosong").style.display = "none";
    let rowData = "";
    if (data.length === 0) {
        document.getElementById("body-table").innerHTML = rowData;
        document.getElementById("table-loading").style.display = "";
        setTimeout(() => {
            document.getElementById("data-kosong").style.display = "";
            document.getElementById("table-loading").style.display = "none";
        }, 1000)
    } else {
        for (let i = 0; i < data.length; data++) {
            data.forEach((d) => {
                let isi = "";
                d.forEach((val) => {
                    isi += `<td>${val}</td>`;
                });
                rowData += `<tr>${isi}</tr>`;
            });
        }
        document.getElementById("body-table").innerHTML = rowData;
        document.getElementById("table-loading").style.display = "none";
        document.getElementById("data-kosong").style.display = "none";
    }
};

export { dataTableUser, headerTableUser };